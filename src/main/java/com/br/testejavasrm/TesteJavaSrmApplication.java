package com.br.testejavasrm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TesteJavaSrmApplication {

    public static void main(String[] args) {
        SpringApplication.run(TesteJavaSrmApplication.class, args);
    }
}
