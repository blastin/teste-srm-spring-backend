package com.br.testejavasrm.servicos;

import com.br.testejavasrm.modelos.Cliente;
import com.br.testejavasrm.modelos.Credito;
import com.br.testejavasrm.modelos.Risco;
import com.br.testejavasrm.repositorios.ClienteRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class ClienteServico {

    private final ClienteRepositorio repositorio;

    @Autowired
    public ClienteServico(ClienteRepositorio repositorio) {
        this.repositorio = repositorio;
    }

    public Collection<Cliente> obterClientes(){
        return repositorio.findAll();
    }

    public void cadastrarCliente(Cliente cliente) {
        calcularTaxaDeJuros(cliente);
        repositorio.save(cliente);
    }

    private void calcularTaxaDeJuros(Cliente cliente){
        Risco risco = cliente.getRisco();
        Credito credito = cliente.getCredito();
        switch (risco){
            case A:credito.setJuros(Credito.TAXA_DE_JUROS_RISCO_A);break;
            case B:credito.setJuros(Credito.TAXA_DE_JUROS_RISCO_B);break;
            case C:credito.setJuros(Credito.TAXA_DE_JUROS_RISCO_C);break;
        }
    }
}
