package com.br.testejavasrm.servicos;

import com.br.testejavasrm.modelos.Risco;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collection;

@Service
public class RiscoServico {

    public Collection<Risco> obterRiscos(){
        return Arrays.asList(Risco.values());
    }
}
