package com.br.testejavasrm.repositorios;

import com.br.testejavasrm.modelos.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClienteRepositorio extends JpaRepository<Cliente, Long> {
}
