package com.br.testejavasrm.modelos;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.util.Objects;

@Entity
@Table(name = "CREDITOS")
public class Credito {

    public final static int TAXA_DE_JUROS_RISCO_A = 0;
    public final static int TAXA_DE_JUROS_RISCO_B = 10;
    public final static int TAXA_DE_JUROS_RISCO_C = 20;

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private long id;

    @Column(name = "LIMITE_CREDITO")
    @NotNull
    private double limite;

    @Column(name = "TAXA_JUROS")
    private double juros;

    public long getId() {
        return id;
    }

    public double getLimite() {
        return limite;
    }

    public void setLimite(double limite) {
        this.limite = limite;
    }

    public double getJuros() {
        return juros;
    }

    public void setJuros(double juros) {
        this.juros = juros;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Credito credito = (Credito) o;
        return id == credito.id &&
                Double.compare(credito.limite, limite) == 0 &&
                Double.compare(credito.juros, juros) == 0;
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, limite, juros);
    }

    @Override
    public String toString() {
        return "Credito{" +
                "id=" + id +
                ", limite=" + limite +
                ", juros=" + juros +
                '}';
    }
}
