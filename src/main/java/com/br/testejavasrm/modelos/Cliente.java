package com.br.testejavasrm.modelos;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
@Table(name = "CLIENTES")
public class Cliente {

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private long id;

    @Column(name = "NOME_COMPLETO")
    @NotNull @NotEmpty
    private String nomeCompleto;

    @OneToOne(cascade = CascadeType.ALL)
    private Credito credito;

    @Enumerated(value = EnumType.STRING)
    private Risco risco;

    public long getId() {
        return id;
    }

    public String getNomeCompleto() {
        return nomeCompleto;
    }

    public void setNomeCompleto(String nomeCompleto) {
        this.nomeCompleto = nomeCompleto;
    }

    public Credito getCredito() {
        return credito;
    }

    public void setCredito(Credito credito) {
        this.credito = credito;
    }

    public Risco getRisco() {
        return risco;
    }

    public void setRisco(Risco risco) {
        this.risco = risco;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cliente cliente = (Cliente) o;
        return id == cliente.id &&
                Objects.equals(nomeCompleto, cliente.nomeCompleto) &&
                Objects.equals(credito, cliente.credito) &&
                risco == cliente.risco;
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, nomeCompleto, credito, risco);
    }

    @Override
    public String toString() {
        return "Cliente{" +
                "id=" + id +
                ", nomeCompleto='" + nomeCompleto + '\'' +
                ", credito=" + credito +
                ", risco=" + risco +
                '}';
    }
}
