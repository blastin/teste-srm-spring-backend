package com.br.testejavasrm.controles;

import com.br.testejavasrm.modelos.Cliente;
import com.br.testejavasrm.servicos.ClienteServico;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "/api/cliente")
public class ClienteControle {

    private final ClienteServico servico;

    @Autowired
    public ClienteControle(ClienteServico servico) {
        this.servico = servico;
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public Collection<Cliente> clientes(){
        return servico.obterClientes();
    }

    @RequestMapping(value = "/cadastrar", method = RequestMethod.POST)
    public ResponseEntity<Number> cadastrar(@RequestBody Cliente cliente){
        servico.cadastrarCliente(cliente);
        return new ResponseEntity<>(HttpStatus.CREATED.value(), HttpStatus.CREATED);
    }

}
