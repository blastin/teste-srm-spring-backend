package com.br.testejavasrm.controles;

import com.br.testejavasrm.modelos.Risco;
import com.br.testejavasrm.servicos.RiscoServico;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "/api/risco")
public class RiscoControle {

    private final RiscoServico servico;

    @Autowired
    public RiscoControle(RiscoServico servico) {
        this.servico = servico;
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public Collection<Risco> riscos(){
        return servico.obterRiscos();
    }

}
